package ua.net.tokar.csdudes.meetup1;

public class Board {
    private int[] paths;

    public int[] getPaths() {
        return paths;
    }

    public Board( int width, int height ) {
        paths = new int[width * height];

        for ( int i = 0; i < paths.length; i++ ) {
            paths[i] = i + 1;
        }
    }

    public void addLadder( int from, int to ) {
        paths[from - 1] = to - 1;
    }

    public void addSnake( int from, int to ) {
        paths[from - 1] = to - 1;
    }
}
