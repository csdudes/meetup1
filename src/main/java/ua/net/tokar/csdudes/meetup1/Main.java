package ua.net.tokar.csdudes.meetup1;

public class Main {
    public static void main( String[] args ) {
        Board board = new Board( 6, 5 );
        board.addLadder( 3, 22 );
        board.addLadder( 5, 8 );
        board.addLadder( 11, 26 );
        board.addLadder( 20, 29 );

        board.addSnake( 17, 4 );
        board.addSnake( 19, 7 );
        board.addSnake( 21, 9 );
        board.addSnake( 27, 1 );

        MinDiceSolver solver = new MinDiceSolver( board );

        System.out.println( "Min dice rolls number is " + solver.getMinDiceRolls() );
    }
}

