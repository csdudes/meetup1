package ua.net.tokar.csdudes.meetup1;

import java.util.LinkedList;
import java.util.Queue;

public class MinDiceSolver {
    private static final int DICE_SIZE = 6;

    private final Board board;
    public MinDiceSolver( Board board ) {
        this.board = board;
    }

    private static class QEntry {
        private int cellid;
        private int distance;

        public QEntry( int cellid, int distance ) {
            this.cellid = cellid;
            this.distance = distance;
        }
    }

    public int getMinDiceRolls() {
        int[] paths = board.getPaths();
        boolean[] visited = new boolean[paths.length];

        QEntry current = new QEntry( 0, 0 );
        visited[0] = true;

        Queue<QEntry> q = new LinkedList<>();
        q.add( current );

        while ( !q.isEmpty() ) {
            current = q.poll();

            if ( current.cellid == paths.length - 1 ) {
                break;
            }

            for ( int i = current.cellid + 1; ( i <= current.cellid + DICE_SIZE ) && i < paths.length; i++ ) {
                if ( !visited[i] ) {
                    q.add( new QEntry( paths[i], current.distance + 1 ) );

                    visited[i] = true;
                }
            }
        }

        return current.distance;
    }
}
